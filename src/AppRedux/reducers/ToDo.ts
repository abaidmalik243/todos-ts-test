
import { ADD_TODO, DELETE_TODO, UPDATE_TODO } from '../../constants/ActionTypes';
import { TodoActionTypes } from '../Models'

interface ITodoModel {
  // count: number;
  list: object[];
}

const initialState: ITodoModel = {
  // count: 2,
  list: [
    {
      id: 1,
      task: "Task 01",
      complete: true,
    },
    {
      id: 2,
      task: "Task 02",
      complete: false,
    }
  ]
};

export default (state: ITodoModel = initialState, action: TodoActionTypes) => {
  switch (action.type) {
    case ADD_TODO: {
      const newItem = {
        id: state.list.length + 1,
        task: action.payload,
        complete: false
      }
      return {
        ...state,
        list: [...state.list, newItem]
      };
    }

    case DELETE_TODO: {
      state.list = state.list.filter((item: any) => item.id !== action.payload);

      return {
        ...state,
        list: [...state.list]
      };
    }

    case UPDATE_TODO: {
      state.list = state.list.map((item: any) => {
        // console.log("check update data ", item.id, action.payload);
        if (item.id === action.payload.id) {
          item = action.payload;
        }
        return item;

      });

      return {
        ...state,
        list: [...state.list]
      };
    }

    default:
      return state;
  }
};