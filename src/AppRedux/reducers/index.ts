import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import Todo from "./ToDo";


const reducers = combineReducers({
  routing: routerReducer,
  todo: Todo
});

export default reducers;
