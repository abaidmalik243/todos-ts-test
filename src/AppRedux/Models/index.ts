import { ADD_TODO, TODO_LIST_CONSTANT, DELETE_TODO, UPDATE_TODO } from '../../constants/ActionTypes';

export interface Todo {
  id: any
  task: any
  complete: boolean
}
// Get Todo List
export interface TodoList {
  todoList: [Todo]
}
interface TodoListPayload {
  type: typeof TODO_LIST_CONSTANT,
  payload: TodoList
}

// Add New Todo item
export interface AddNewTodo {
  addNewTodo: any
}

interface AddNewTodoPayload {
  type: typeof ADD_TODO
  payload: AddNewTodo
}

// Delete Todo item
export interface DeleteTodo {
  deleteTodo: any
}

interface DeleteTodoPayload {
  type: typeof DELETE_TODO
  payload: DeleteTodo
}

interface UpdateTodoPayload {
  type: typeof UPDATE_TODO
  payload: Todo
}

export type TodoActionTypes = AddNewTodoPayload | DeleteTodoPayload | UpdateTodoPayload | TodoListPayload