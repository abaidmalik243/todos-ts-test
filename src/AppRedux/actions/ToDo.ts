import { TODO_LIST_CONSTANT, ADD_TODO, DELETE_TODO, UPDATE_TODO } from '../../constants/ActionTypes';
import { TodoList, AddNewTodo, DeleteTodo, Todo, TodoActionTypes } from '../Models'
import { ThunkAction, ThunkDispatch } from 'redux-thunk';

export const todoListAction = (todoList: TodoList): ThunkAction<Promise<void>, {}, {}, TodoActionTypes> => {
	// Invoke API
	return async (dispatch: ThunkDispatch<{}, {}, TodoActionTypes>): Promise<void> => {
		// console.log("hello todoListAction")
		dispatch({
			payload: todoList,
			type: TODO_LIST_CONSTANT
		})


	}
}

export const addNewTodoAction = (addNewTodo: AddNewTodo): ThunkAction<Promise<void>, {}, {}, TodoActionTypes> => {
	// Invoke API
	return async (dispatch: ThunkDispatch<{}, {}, TodoActionTypes>): Promise<void> => {
		// console.log("hello addNewTodoAction")
		dispatch({
			payload: addNewTodo,
			type: ADD_TODO
		})
	}
}

export const deleteTodoAction = (deleteTodo: DeleteTodo): ThunkAction<Promise<void>, {}, {}, TodoActionTypes> => {
	// Invoke API
	return async (dispatch: ThunkDispatch<{}, {}, TodoActionTypes>): Promise<void> => {
		// console.log("hello deleteTodoAction")
		dispatch({
			payload: deleteTodo,
			type: DELETE_TODO
		})
	}
}


export const updateTodoAction = (updateTodo: Todo): ThunkAction<Promise<void>, {}, {}, TodoActionTypes> => {
	// Invoke API
	return async (dispatch: ThunkDispatch<{}, {}, TodoActionTypes>): Promise<void> => {
		// console.log("hello updateTodoAction", updateTodo)
		dispatch({
			payload: updateTodo,
			type: UPDATE_TODO
		})
	}
}

