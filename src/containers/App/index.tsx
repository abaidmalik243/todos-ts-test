import * as React from 'react';
import {
  // List, 
  Input, Button, Table
} from 'antd';
// import {IRecipeProps, IRecipeState } from './app'
import { todoListAction, addNewTodoAction, deleteTodoAction, updateTodoAction } from '../../AppRedux/actions'
import { bindActionCreators } from "redux";
import { connect } from 'react-redux'


interface IProps {
  todoList: any,
  todoListAction: any,
  addNewTodoAction: any,
  deleteTodoAction: any,
  updateTodoAction: any
}
interface IState {
  newTodoItem?: any;
  editTask?: any
}

class App extends React.Component<IProps, IState> {

  constructor(prop: IProps) {
    super(prop);
    this.state = {
      newTodoItem: '',
      editTask: ''
    }
  }

  public componentDidMount() {
    this.props.todoListAction();
  }

  public newItem = (e: any) => {
    // console.log("at new item e.target.value ", e.target.value)
    this.setState({
      newTodoItem: e.target.value
    })
  }

  public updateItemOnChange = (e: any) => {
    // console.log("at update e.target.value ", e.target.value)
    const copyItem = this.state.editTask;
    copyItem.task = e.target.value;

    this.setState({
      editTask: copyItem
    })
  }

  public deleteTodo = (id: any) => {
    this.props.deleteTodoAction(id);
  }

  public editTodo = (data: any) => {
    this.setState({ editTask: data })
  }

  public handleUpdate = () => {
    this.props.updateTodoAction(this.state.editTask);
    this.setState({ editTask: '' })
  }

  public handlenNewItem = () => {
    this.props.addNewTodoAction(this.state.newTodoItem);

    this.setState({
      newTodoItem: ''
    })
  }



  public renderTodoList = (data: any) => {
    // console.log("list ", data)
    return data.map((item: any) => {
      if (this.state.editTask && item.id === this.state.editTask.id) {
        return {
          key: item.id,
          action: <React.Fragment>
            <Input onChange={this.updateItemOnChange} value={this.state.editTask.task} placeholder="Update item..." />
            <Button type="primary" onClick={this.handleUpdate}>UPDATE</Button>
          </React.Fragment>,
          task: this.state.editTask.task
        }
      } else {
        return {
          key: item.id,
          action: <React.Fragment>
            <Button type="danger" size="small" onClick={() => this.deleteTodo(item.id)}>DELETE</Button>
            <Button type="dashed" size="small" onClick={() => this.editTodo(item)}>EDIT</Button>
          </React.Fragment>,
          task: item.task
        }
      }
    })
  }

  public render() {
    return (
      <div className="container">
        <h2>Todo App</h2>
        <Input onChange={this.newItem} value={this.state.newTodoItem} placeholder="Add item..." />
        <Button type="default" onClick={this.handlenNewItem}>Add</Button>
        <Table
          style={{ width: '100%' }}
          columns={[
            {
              title: "#",
              dataIndex: 'counter',
              align: 'center',
              className: 'row',
              render: (text: any, record: any, index: any) => ++index,
            },
            {
              title: "ACTION",
              dataIndex: 'action',
              align: 'center',
              className: '',
            },
            {
              title: "TASKS",
              dataIndex: 'task',
              align: 'center',
              className: '',
            }
          ]}
          bordered={true}
          dataSource={this.renderTodoList(this.props.todoList)}
          pagination={false}
        />
      </div >
    )
  }
}

const mapStateToProps = (states: any, ownProps: any) => {
  // console.log(states.todo.list);
  return {
    todoList: states.todo.list
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators({
    todoListAction,
    addNewTodoAction,
    deleteTodoAction,
    updateTodoAction
  }, dispatch)

}

export default connect(mapStateToProps, mapDispatchToProps)(App)